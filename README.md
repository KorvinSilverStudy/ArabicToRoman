### Arabic To Roman
Calculates the [Roman numeral](https://en.wikipedia.org/wiki/Roman_numerals) equivalent of a positive integer up to 9999.
