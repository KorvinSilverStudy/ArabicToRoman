/*
 * Copyright 2018, Korvin F. Ezüst
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package eu.korvin;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Project: Arabic to Roman
 * Created by Korvin F. Ezüst on 2018-03-28
 *
 * Calculates the Roman numeral equivalent of a positive integer up to 9999
 */
public class ArabicToRoman
{
    /** Prints out the Roman numeral i */
    public static void main(String[] args)
    {
        // Ask for user input
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number (1-9999): ");

        try
        {
            // get integer
            int num = scanner.nextInt();
            scanner.close();

            // return if input is zero or less
            if (num <= 0)
            {
                System.out.println("Only non-zero positive integers are allowed!");
                return;
            }
            // return if input is 10_000 or larger
            if (num > 9999)
            {
                System.out.println("Number too large! What's the point? It's just a lot of “M”s.");
                return;
            }

            // print Roman numeral
            System.out.printf("Roman numeral:           %s\n", toRoman(num));
        }
        catch (InputMismatchException e)
        {
            System.out.println("Invalid input! Not an integer.");
        }
    }

    /**
     * Calculates the Roman numeral from a given integer
     *
     * @param i    integer input
     * @param nine 9, 90, or 900 as Roman numeral: IX, XC, or CM
     * @param five 5, 50, or 500 as Roman numeral: V, L, or D
     * @param four 4, 40, or 400 as Roman numeral: IV, XL, or CD
     * @param one  1, 10, or 100 as Roman numeral: I, X, or C
     * @return Roman numeral
     */
    private static String units(int i, String nine, String five, String four, String one)
    {
        if (i >= 9)
            return nine;
        if (i >= 5)
            // five + one * (i-5)
            return five + new String(new char[i - 5]).replace("\0", one);
        if (i == 4)
            return four;
        if (i >= 1)
            // one * i
            return new String(new char[i]).replace("\0", one);
        return "";
    }

    /**
     * Calculates a partial Roman numeral between 1-9 using the units() method
     *
     * @param i integer between 1-9
     * @return partial Roman numeral
     */
    private static String ones(int i)
    {
        return units(i, "IX", "V", "IV", "I");
    }

    /**
     * Calculates a partial Roman numeral from tens between 10-90 using the units() method
     *
     * @param i integer in the tens
     * @return partial Roman numeral
     */
    private static String tens(int i)
    {
        return units(i, "XC", "L", "XL", "X");
    }

    /**
     * Calculates a partial Roman numeral from hundreds between 100-900 using the units() method
     *
     * @param i integer in the hundreds
     * @return partial Roman numeral
     */
    private static String hundreds(int i)
    {
        return units(i, "CM", "D", "CD", "C");
    }

    /**
     * Calculates a partial Roman numeral from thousands
     *
     * @param i integer in the thousands
     * @return partial Roman numeral
     */
    private static String thousands(int i)
    {
        if (i >= 1)
            return new String(new char[i]).replace("\0", "M");
        return "";
    }

    /**
     * Calculates a Roman numeral from a given integer
     *
     * @param i integer
     * @return Roman numeral
     */
    private static String toRoman(int i)
    {
        int    count;
        String roman = "";

        // Count the number of thousands, get partial Roman numeral and subtract the thousands from the integer
        if (i >= 1000)
        {
            count = i / 1000;
            roman += thousands(count);
            i -= count * 1000;
        }

        // As above, but with hundreds
        if (i >= 100)
        {
            count = i / 100;
            roman += hundreds(count);
            i -= count * 100;
        }

        // As above, but with tens
        if (i >= 10)
        {
            count = i / 10;
            roman += tens(count);
            i -= count * 10;
        }

        // As above, but with ones
        if (i >= 1)
            roman += ones(i);

        return roman;
    }
}
